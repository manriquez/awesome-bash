# Awesome-Bash

<h3 align="center">
<img src="https://cdn.rawgit.com/odb/official-bash-logo/master/assets/Logos/Identity/PNG/BASH_logo-transparent-bg-color.png">
</h3>

> Aliases for bash. Very useful for repetitive tasks.


## Creating a Barshrc File

1. Go to home directory    
   `cd ~` 
2. Create bashrc file    
   `touch .bashrc`   
3. Use your favorite editor to update your bashrc file. For example:      
   `vim .bashrc`


## Using Aliases

After creating and updating your .bashrc file, you should now be able to use 
custom commands and aliases in bash terminal. 
> NOTE: You might have to exit your current terminal session the first time you do this. 

After that and if you include the file in this repo, you can just use `reload`
everytime a change is made to the file. I have included here aliases which I have
found the most useful over the years.


#### Reload
Loads the bashrc file again. Useful for when you make changes to the file and 
need to use them without exiting the terminal. 